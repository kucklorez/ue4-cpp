﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "some voice\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo\n";
    }
};

int main()
{
    Animal *animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Cow();

    for (auto animal : animals)
    {
        animal->Voice();
    }
}
